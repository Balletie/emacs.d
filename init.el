;;; init.el --- user-init-file                    -*- lexical-binding: t -*-
;;; Early birds
(progn ;     startup
  (defvar before-user-init-time (current-time)
    "Value of `current-time' when Emacs begins loading `user-init-file'.")
  (message "Loading Emacs...done (%.3fs)"
           (float-time (time-subtract before-user-init-time
                                      before-init-time)))
  (setq user-init-file (or load-file-name buffer-file-name))
  (setq user-emacs-directory (file-name-directory user-init-file))
  (message "Loading %s..." user-init-file)
  (when (< emacs-major-version 27)
    (setq package-enable-at-startup nil)
    ;; (package-initialize)
    (load-file (expand-file-name "early-init.el" user-emacs-directory)))
  (setq inhibit-startup-buffer-menu t)
  (setq inhibit-startup-screen t)
  (setq inhibit-startup-echo-area-message "locutus")
  (setq initial-buffer-choice t)
  (setq initial-scratch-message "")
  (when (fboundp 'scroll-bar-mode)
    (scroll-bar-mode 0))
  (when (fboundp 'tool-bar-mode)
    (tool-bar-mode 0))
  (menu-bar-mode 0))

(load-file (expand-file-name "elpaca-bootstrap.el" user-emacs-directory))

(progn ;    `use-package'
  (require  'use-package)
  (setq use-package-verbose t))

(use-package browse-url
  :config
  (defun browse-url--non-html-file-url-p (url)
    "Return non-nil if URL is a file:// URL of a non-HTML file."
    (and (string-match-p "\\`file://" url)
         (not (string-match-p "\\`file://.*\\.html?\\b" url)))))

;; Ensure compat repo can be found
(use-package compat
  :ensure (:host github :repo "emacs-compat/compat" :branch main))

(use-package dash
  :ensure t)

(use-package dashboard
  :ensure t
  :config
  (dashboard-setup-startup-hook)
  :custom
  (initial-buffer-choice (lambda () (get-buffer-create "*dashboard*")))
  (dashboard-items '((recents  . 5)
                     (bookmarks . 5)
                     (projects . 5)
                     (registers . 5)))
  (dashboard-startup-banner 'logo)
  (dashboard-center-content t)
  (dashboard-page-separator "\n\f\n")
  (dashboard-set-footer nil))

(use-package diminish
  :ensure t)

(use-package no-littering
  :ensure t
  :demand t)

(use-package custom
  :requires (no-littering)
  :no-require t
  :config
  (setq custom-file (no-littering-expand-etc-file-name "custom.el"))
  (when (file-exists-p custom-file)
    (load custom-file)))

(use-package server
  :commands (server-running-p)
  :config (or (server-running-p) (server-mode)))

(progn ;     startup
  (message "Loading early birds...done (%.3fs)"
           (float-time (time-subtract (current-time)
                                      before-user-init-time))))

(progn ; `map-ynp'
  ;; Make all "yes or no" prompts show "y or n" instead
  (setq read-answer-short t)
  (fset 'yes-or-no-p 'y-or-n-p))

(use-package modus-themes
  :ensure t
  :demand t
  :bind ("<f5>" . modus-themes-toggle)
  :custom
  (modus-themes-to-toggle '(modus-operandi-tinted modus-vivendi-tinted))
  :init
  (setq modus-themes-italic-constructs t
        modus-themes-slanted-constructs t
        modus-themes-bold-constructs t
        modus-themes-mixed-fonts t
        modus-themes-headings '((0 . (1.5))
                                (1 . (1.3))
                                (t . (1.1)))
        modus-themes-completions '((matches . (extrabold background intense))
                                   (selection . (semibold accented intense))
                                   (popup . (accented)))
        modus-themes-common-palette-overrides '((border-mode-line-active unspecified)
                                                (border-mode-line-inactive unspecified)))
  :config
  (declare-function modus-themes-load-theme "modus-themes.el")
  (modus-themes-load-theme 'modus-vivendi-tinted))

(use-package frame
  :config
  (setq frame-title-format "Emacs (%b)")
  (set-face-attribute 'default nil :family "Iosevka Term" :width 'regular :height 120)
  (set-face-attribute 'fixed-pitch nil :family (face-attribute 'default :family))
  (set-face-attribute 'variable-pitch nil :family "Iosevka Aile"))

(use-package moody
  :ensure t
  :demand t
  :config
  ;; Adapted from Damien's init.el
  (defvar my/mode-line-mule-info
    '(:eval (unless (eq buffer-file-coding-system
                        (default-value 'buffer-file-coding-system))
              (list mode-line-mule-info " "))))
  (put 'my/mode-line-mule-info 'risky-local-variable t)
  (make-local-variable 'my/mode-line-mule-info)

  (defvar my/mode-line-position
    (list mode-line-percent-position " %l:%c "))

  (defvar my/mode-line-buffer-identification
    '(:eval (moody-tab (let* ((mode-line (propertized-buffer-identification "%b"))
                              (mode-line (format-mode-line mode-line)))
                         (when (and buffer-file-name buffer-read-only)
                           (add-face-text-property 0 (length mode-line) '(:foreground "red")
                                                   nil mode-line))
                         (when (buffer-modified-p (current-buffer))
                           (add-face-text-property 0 (length mode-line) '(:slant italic)
                                                   nil mode-line))
                         mode-line)
                       20 'down)))
  (put 'my/mode-line-buffer-identification 'risky-local-variable t)
  (make-local-variable 'my/mode-line-buffer-identification)

  (setq x-underline-at-descent-line t)

  ;(moody-replace-mode-line-buffer-identification)
  (declare-function moody-replace-element "moody.el")
  (moody-replace-element 'mode-line-mule-info 'my/mode-line-mule-info)
  (moody-replace-element 'mode-line-position 'my/mode-line-position)
  (moody-replace-element 'mode-line-buffer-identification 'my/mode-line-buffer-identification)
  (moody-replace-vc-mode)
  (setq mode-line-format (delete '(vc-mode moody-vc-mode) mode-line-format))
  (setq mode-line-format (delq 'mode-line-remote mode-line-format))
  (setq mode-line-format (delq 'mode-line-modified mode-line-format))
  (setq mode-line-format (delq 'mode-line-client mode-line-format)))

;;; Long tail

(use-package avy
  :ensure t
  :bind (("C-;" . avy-goto-word-1)
	 ("C-'" . avy-isearch)))

(use-package auto-dark
  :ensure t
  :diminish t
  :custom
  (auto-dark-dark-theme 'modus-vivendi)
  (auto-dark-light-theme 'modus-operandi)
  :config (auto-dark-mode t))

(use-package autorevert
  :diminish auto-revert-mode)

(use-package breadcrumb
  :ensure t
  :config (breadcrumb-mode 1))

(use-package caddyfile-mode
  :ensure t
  :mode (("CaddyFile\\'" . caddyfile-mode)
         ("caddy\\.conf\\'" . caddyfile-mode)))

(use-package consult
  :ensure t
  :demand
  ;; Replace bindings. Lazily loaded due by `use-package'.
  :bind (;; C-c bindings (mode-specific-map)
         ("C-c h" . consult-history)
         ("C-c m" . consult-mode-command)
         ("C-c k" . consult-kmacro)
         ;; C-x bindings (ctl-x-map)
         ("C-x M-:" . consult-complex-command)     ;; orig. repeat-complex-command
         ("C-x b" . consult-buffer)                ;; orig. switch-to-buffer
         ("C-x 4 b" . consult-buffer-other-window) ;; orig. switch-to-buffer-other-window
         ("C-x 5 b" . consult-buffer-other-frame)  ;; orig. switch-to-buffer-other-frame
         ("C-x r b" . consult-bookmark)            ;; orig. bookmark-jump
         ("C-x p b" . consult-project-buffer)      ;; orig. project-switch-to-buffer
         ;; Custom M-# bindings for fast register access
         ("M-#" . consult-register-load)
         ("M-'" . consult-register-store)          ;; orig. abbrev-prefix-mark (unrelated)
         ("C-M-#" . consult-register)
         ;; Other custom bindings
         ("M-y" . consult-yank-pop)                ;; orig. yank-pop
         ("<help> a" . consult-apropos)            ;; orig. apropos-command
         ;; M-g bindings (goto-map)
         ("M-g e" . consult-compile-error)
         ("M-g f" . consult-flymake)               ;; Alternative: consult-flycheck
         ("M-g g" . consult-goto-line)             ;; orig. goto-line
         ("M-g M-g" . consult-goto-line)           ;; orig. goto-line
         ("M-g o" . consult-outline)               ;; Alternative: consult-org-heading
         ("M-g m" . consult-mark)
         ("M-g k" . consult-global-mark)
         ("M-g i" . consult-imenu)
         ("M-g I" . consult-imenu-multi)
         ;; M-s bindings (search-map)
         ("M-s d" . consult-find)
         ("M-s D" . consult-locate)
         ("M-s g" . consult-grep)
         ("M-s G" . consult-git-grep)
         ("M-s r" . consult-ripgrep)
         ("M-s a" . consult-ag)
         ("M-s l" . consult-line)
         ("M-s L" . consult-line-multi)
         ("M-s m" . consult-multi-occur)
         ("M-s k" . consult-keep-lines)
         ("M-s u" . consult-focus-lines)
         ;; Isearch integration
         ("M-s e" . consult-isearch-history)
         :map isearch-mode-map
         ("M-e" . consult-isearch-history)         ;; orig. isearch-edit-string
         ("M-s e" . consult-isearch-history)       ;; orig. isearch-edit-string
         ("M-s l" . consult-line)                  ;; needed by consult-line to detect isearch
         ("M-s L" . consult-line-multi)            ;; needed by consult-line to detect isearch
         ;; Minibuffer history
         :map minibuffer-local-map
         ("M-s" . consult-history)                 ;; orig. next-matching-history-element
         ("M-r" . consult-history))                ;; orig. previous-matching-history-element

  ;; Enable automatic preview at point in the *Completions* buffer. This is
  ;; relevant when you use the default completion UI.
  :hook (completion-list-mode . consult-preview-at-point-mode)

  :init
  ;; Optionally configure the register formatting. This improves the register
  ;; preview for `consult-register', `consult-register-load',
  ;; `consult-register-store' and the Emacs built-ins.
  (setq register-preview-delay 0.5
        register-preview-function #'consult-register-format)

  ;; Optionally tweak the register preview window.
  ;; This adds thin lines, sorting and hides the mode line of the window.
  (advice-add #'register-preview :override #'consult-register-window)

  :config
  ;; Optionally configure preview. The default value
  ;; is 'any, such that any key triggers the preview.
  ;; (setq consult-preview-key 'any)
  ;; (setq consult-preview-key (kbd "M-."))
  ;; (setq consult-preview-key (list (kbd "<S-down>") (kbd "<S-up>")))
  ;; For some commands and buffer sources it is useful to configure the
  ;; :preview-key on a per-command basis using the `consult-customize' macro.
  (consult-customize
   consult-theme :preview-key '(:debounce 0.2 any)
   consult-ripgrep consult-git-grep consult-grep
   consult-bookmark consult-recent-file
   consult--source-bookmark consult--source-file-register
   consult--source-recent-file consult--source-project-recent-file
   :preview-key '(:debounce 0.4 any))

  ;; Optionally configure the narrowing key.
  ;; Both < and C-+ work reasonably well.
  (setq consult-narrow-key "<") ;; (kbd "C-+")
)

(use-package consult-dir
  :ensure t
  :bind (("C-x C-d" . consult-dir)
         :map vertico-map
         ("C-x C-d" . consult-dir)
         ("C-x C-j" . consult-dir-jump-file)))

(use-package consult-ag
  :ensure t)

(use-package devdocs
  :ensure t
  :bind (("C-h D" . devdocs-lookup)))

(use-package diff-hl
  :ensure t
  :config
  (setq diff-hl-draw-borders nil)
  (global-diff-hl-mode)
  (add-hook 'magit-post-refresh-hook 'diff-hl-magit-post-refresh t))

(use-package diff-mode
  :defer t
  :config
  (when (>= emacs-major-version 27)
    (set-face-attribute 'diff-refine-changed nil :extend t)
    (set-face-attribute 'diff-refine-removed nil :extend t)
    (set-face-attribute 'diff-refine-added   nil :extend t)))

(use-package dired
  :defer t
  :config (setq dired-listing-switches "-alh"))

(use-package direnv
  :ensure t
  :defer t
  :config
  (setq direnv-always-show-summary t
	direnv-show-paths-in-summary nil)
  (declare-function direnv--maybe-update-environment "direnv.el")
  (add-hook 'prog-mode-hook #'direnv--maybe-update-environment)
  (add-hook 'comint-mode-hook #'direnv--maybe-update-environment)
  (direnv-mode))

(use-package display-line-numbers
  :hook ((prog-mode text-mode) . display-line-numbers-mode))

(use-package dpkg-dev-el
  :ensure t)

(use-package edraw
  :ensure (edraw :type git :host github :repo "misohena/el-easydraw"))

(use-package eglot
  :defer t
  :hook ((python-mode python-ts-mode) . eglot-ensure)
  :config
  (setq-default eglot-workspace-configuration
                '((:pylsp . (:plugins (
                                       :pycodestyle (:enabled :json-false)
                                       :mccabe (:enabled :json-false)
                                       :pyflakes (:enabled :json-false)
                                       :flake8 (:enabled :json-false)
                                       :ruff (:enabled t)
                                       :yapf (:enabled :json-false)
                                       :autopep8 (:enabled :json-false)
                                       :black (:enabled :json-false)))))))

(use-package eldoc
  :when (version< "25" emacs-version)
  :config (global-eldoc-mode))

(use-package embark
  :ensure t
  :bind
  (("C-." . embark-act)         ;; pick some comfortable binding
   ("M-." . embark-dwim)        ;; good alternative: M-.
   ("C-h B" . embark-bindings)) ;; alternative for `describe-bindings'

  :init

  ;; Optionally replace the key help with a completing-read interface
  (setq prefix-help-command #'embark-prefix-help-command)

  ;; Show the Embark target at point via Eldoc. You may adjust the
  ;; Eldoc strategy, if you want to see the documentation from
  ;; multiple providers. Beware that using this can be a little
  ;; jarring since the message shown in the minibuffer can be more
  ;; than one line, causing the modeline to move up and down:

  ;; (add-hook 'eldoc-documentation-functions #'embark-eldoc-first-target)
  ;; (setq eldoc-documentation-strategy #'eldoc-documentation-compose-eagerly)

  :config

  ;; Hide the mode line of the Embark live/completions buffers
  (add-to-list 'display-buffer-alist
               '("\\`\\*Embark Collect \\(Live\\|Completions\\)\\*"
                 nil
                 (window-parameters (mode-line-format . none)))))

;; Consult users will also want the embark-consult package.
(use-package embark-consult
  :ensure t ; only need to install it, embark loads it after consult if found
  :hook
  (embark-collect-mode . consult-preview-at-point-mode))

(use-package gl-conf-mode
  :ensure t
  :defer t)

(use-package git-annex
  :ensure t)

(use-package help
  :defer t
  :config (temp-buffer-resize-mode))

(progn ;    `isearch'
  (setq isearch-allow-scroll t))

(use-package jinja2-mode
  :ensure t
  :defer t
  :config
  (setq indent-tabs-mode nil))

(use-package lisp-mode
  :config
  (add-hook 'emacs-lisp-mode-hook 'outline-minor-mode)
  (add-hook 'emacs-lisp-mode-hook 'reveal-mode))

(elpaca transient)
(use-package magit
  :ensure t
  :demand t
  :commands (magit-add-section-hook)
  :bind (("C-x g" . magit-status) ("C-x G" . magit-dispatch))
  :config
  (magit-add-section-hook 'magit-status-sections-hook
                          'magit-insert-modules
                          'magit-insert-stashes
                          'append))

(use-package magit-annex
  :ensure t
  :after magit
  :config
  (transient-define-argument magit-annex:--content-of ()
    :description "Transfer content of"
    :class 'transient-files
    :key "=c"
    :argument "--content-of="
    :prompt "Transfer content of file or directory: "
    :reader #'magit-read-files
    :multi-value 'repeat)
  (transient-append-suffix 'magit-annex-sync "-c"
    '(magit-annex:--content-of)))

(use-package make-mode
  :hook
  (makefile-mode . indent-tabs-mode))

(use-package man
  :defer t
  :config (setq Man-width 80))

(use-package marginalia
  :ensure t
  :demand t
  :bind (
         :map minibuffer-local-map
         ("M-A" . marginalia-cycle))
  :config
  (progn
    (marginalia-mode)))

(use-package markdown-mode
  :ensure t
  :commands (markdown-mode gfm-mode)
  :mode (("README\\.md\\'" . gfm-mode)
         ("\\.md\\'" . markdown-mode)
         ("\\.mdwn\\'" . markdown-mode)
         ("\\.markdown\\'" . markdown-mode)))

;; Ensure mathjax repo can be found (dep of devdocs)
(use-package mathjax
  :ensure (:type git :host github :repo "astoff/mathjax.el" :branch main))

(use-package meow
  :ensure t
  :diminish (meow-normal-mode meow-insert-mode meow-beacon-mode meow-motion-mode meow-keypad-mode)
  :custom
  (meow-expand-hint-counts '((word . 20)
                             (line . 0)
                             (block . 20)
                             (find . 20)
                             (till . 20)))
  :config
  (defun meow-setup ()
    (setq meow-cheatsheet-layout meow-cheatsheet-layout-qwerty)
    (meow-motion-overwrite-define-key
     '("j" . meow-next)
     '("k" . meow-prev)
     '("<escape>" . ignore))
    (meow-leader-define-key
     ;; SPC j/k will run the original command in MOTION state.
     '("j" . "H-j")
     '("k" . "H-k")
     ;; Use SPC (0-9) for digit arguments.
     '("1" . meow-digit-argument)
     '("2" . meow-digit-argument)
     '("3" . meow-digit-argument)
     '("4" . meow-digit-argument)
     '("5" . meow-digit-argument)
     '("6" . meow-digit-argument)
     '("7" . meow-digit-argument)
     '("8" . meow-digit-argument)
     '("9" . meow-digit-argument)
     '("0" . meow-digit-argument)
     '("/" . meow-keypad-describe-key)
     '("?" . meow-cheatsheet))
    (meow-normal-define-key
     '("0" . meow-expand-0)
     '("9" . meow-expand-9)
     '("8" . meow-expand-8)
     '("7" . meow-expand-7)
     '("6" . meow-expand-6)
     '("5" . meow-expand-5)
     '("4" . meow-expand-4)
     '("3" . meow-expand-3)
     '("2" . meow-expand-2)
     '("1" . meow-expand-1)
     '("-" . negative-argument)
     '(";" . meow-reverse)
     '("," . meow-inner-of-thing)
     '("." . meow-bounds-of-thing)
     '("[" . meow-beginning-of-thing)
     '("]" . meow-end-of-thing)
     '("a" . meow-append)
     '("A" . meow-open-below)
     '("b" . meow-back-word)
     '("B" . meow-back-symbol)
     '("c" . meow-change-save)
     '("d" . meow-delete)
     '("D" . meow-backward-delete)
     '("e" . meow-next-word)
     '("E" . meow-next-symbol)
     '("f" . meow-find)
     '("g" . meow-cancel-selection)
     '("G" . meow-grab)
     '("h" . meow-left)
     '("H" . meow-left-expand)
     '("i" . meow-insert)
     '("I" . meow-open-above)
     '("j" . meow-next)
     '("J" . meow-next-expand)
     '("k" . meow-prev)
     '("K" . meow-prev-expand)
     '("l" . meow-right)
     '("L" . meow-right-expand)
     '("m" . meow-join)
     '("n" . meow-search)
     '("o" . meow-block)
     '("O" . meow-to-block)
     '("p" . meow-yank)
     '("q" . meow-quit)
     '("Q" . meow-goto-line)
     '("r" . meow-replace)
     '("R" . meow-swap-grab)
     '("s" . meow-kill)
     '("t" . meow-till)
     '("u" . meow-undo)
     '("U" . meow-undo-in-selection)
     '("v" . meow-visit)
     '("w" . meow-mark-word)
     '("W" . meow-mark-symbol)
     '("x" . meow-line)
     '("X" . meow-goto-line)
     '("y" . meow-save)
     '("Y" . meow-sync-grab)
     '("z" . meow-pop-selection)
     '("'" . repeat)
     '("<escape>" . ignore))
    (meow-setup-indicator)
    (meow-setup-line-number))
  (meow-setup)
  (meow-global-mode 1))

(use-package nix-mode
  :ensure t
  :mode ("\\.nix\\'"))

(use-package orderless
  :ensure t
  :demand t
  :custom
  (completion-styles '(orderless basic))
  (completion-category-overrides '((file (styles partial-completion)))))

(use-package org
  :demand t)

(use-package org-modern
  :ensure t
  :after org
  :init
  (add-hook 'org-modern-mode-hook 'variable-pitch-mode)
  (add-hook 'org-modern-mode-hook 'visual-line-mode)
  :config
  (setq
   ;; Edit settings
   org-auto-align-tags nil
   org-tags-column 0
   org-catch-invisible-edits 'show-and-error
   org-special-ctrl-a/e t
   org-insert-heading-respect-content t

   ;; Org styling, hide markup etc.
   org-hide-emphasis-markers t
   org-pretty-entities t
   org-ellipsis "…")
  (set-face-attribute 'org-ellipsis nil :inherit 'default :box nil)
  (set-face-attribute 'org-modern-symbol nil :family "Iosevka")
  (global-org-modern-mode))

(use-package page-break-lines
  :ensure t
  :diminish t
  :custom
  (page-break-lines-char ?⋅)
  (page-break-lines-modes '(emacs-lisp-mode lisp-mode scheme-mode
                            compilation-mode outline-mode help-mode
                            makefile-mode))
  :config
  (global-page-break-lines-mode))

(use-package paren
  :config (show-paren-mode))

(use-package pet
  :ensure t
  :preface
  (defun enable-pylsp ()
    ;; Python interpreter
    (setq-local python-shell-interpreter (pet-executable-find "python")
                python-shell-virtualenv-root (pet-virtualenv-root))
    ;; Pytest (for python-pytest)
    ;;(setq-local python-pytest-executable (pet-executable-find "pytest"))
    ;; Eglot
    (require 'eglot)
    (setq-local eglot-server-programs
                (cons `((python-mode python-ts-mode)
                        . (,(pet-executable-find "pylsp")))
                      eglot-server-programs)))
  :hook ((python-mode python-ts-mode) . enable-pylsp))

(use-package popper
  :ensure t
  :bind (("C-`"   . popper-toggle)
         ("M-`"   . popper-cycle)
         ("C-M-`" . popper-toggle-type))
  :init
  (setq popper-reference-buffers
        '("\\*Messages\\*"
          "Output\\*$"
          "\\*Async Shell Command\\*"
          help-mode
          compilation-mode))
  (popper-mode +1)
  (popper-echo-mode +1))

(use-package prog-mode
  :config
  (defun indent-spaces-mode ()
    (setq-local indent-tabs-mode nil))
  (add-hook 'prog-mode-hook 'indent-spaces-mode)
  (defun indicate-buffer-boundaries-left ()
    (setq indicate-buffer-boundaries 'left))
  (add-hook 'prog-mode-hook 'indicate-buffer-boundaries-left))

(use-package recentf
  :after no-littering
  :demand t
  :config
  (add-to-list 'recentf-exclude "^/\\(?:ssh\\|su\\|sudo\\)?:")
  (recentf-mode 1))

(use-package savehist
  :init (savehist-mode 1))

(use-package saveplace
  :when (version< "25" emacs-version)
  :config (save-place-mode))

(use-package show-font
  :ensure t)

(use-package simple
  :config (column-number-mode))

(use-package smerge-mode
  :defer t
  :config
  (when (>= emacs-major-version 27)
    (set-face-attribute 'smerge-refined-removed nil :extend t)
    (set-face-attribute 'smerge-refined-added   nil :extend t)))

(use-package solaire-mode
  :ensure t
  :config
  (solaire-global-mode 1))

(use-package spacious-padding
  :ensure t
  :config
  (spacious-padding-mode 1))

(progn ;    `text-mode'
  (add-hook 'text-mode-hook 'indicate-buffer-boundaries-left))

(use-package tramp
  :defer t
  :config
  (add-to-list 'tramp-default-proxies-alist '(nil "\\`root\\'" "/ssh:%h:"))
  (add-to-list 'tramp-default-proxies-alist '("localhost" nil nil))
  (add-to-list 'tramp-default-proxies-alist
               (list (regexp-quote (system-name)) nil nil))
  (setq vc-ignore-dir-regexp
        (format "\\(%s\\)\\|\\(%s\\)"
                vc-ignore-dir-regexp
                tramp-file-name-regexp)))

(use-package tramp-sh
  :defer t
  :config (cl-pushnew 'tramp-own-remote-path tramp-remote-path))

(use-package treesit
  :preface
  (add-to-list 'major-mode-remap-alist
               '((python-mode . python-ts-mode)
                 (css-mode . css-ts-mode)
                 (js2-mode . js-ts-mode)
                 (bash-mode . bash-ts-mode)
                 (conf-toml-mode . toml-ts-mode)
                 (css-mode . css-ts-mode)
                 (json-mode . json-ts-mode)))
  :config
  (dolist (grammar
           ;; Note the version numbers. These are the versions that
           ;; are known to work with Combobulate *and* Emacs.
           '((css . ("https://github.com/tree-sitter/tree-sitter-css" "v0.20.0"))
             (html . ("https://github.com/tree-sitter/tree-sitter-html" "v0.20.1"))
             (javascript . ("https://github.com/tree-sitter/tree-sitter-javascript" "v0.20.1" "src"))
             (json . ("https://github.com/tree-sitter/tree-sitter-json" "v0.20.2"))
             (markdown . ("https://github.com/ikatyang/tree-sitter-markdown" "v0.7.1"))
             (python . ("https://github.com/tree-sitter/tree-sitter-python" "v0.20.4"))
             (toml . ("https://github.com/tree-sitter/tree-sitter-toml" "v0.5.1"))
             (yaml . ("https://github.com/ikatyang/tree-sitter-yaml" "v0.5.0"))))
    (add-to-list 'treesit-language-source-alist grammar)
    ;; Only install `grammar' if we don't already have it
    ;; installed. However, if you want to *update* a grammar then
    ;; this obviously prevents that from happening.
    (unless (treesit-language-available-p (car grammar))
      (treesit-install-language-grammar (car grammar)))))

(use-package vertico
  :ensure t
  :config
  (vertico-mode))

(use-package which-key
  :ensure t
  :demand
  :diminish
  :bind (("C-h K" . which-key-show-full-major-mode)
         ("C-h C-k" . which-key-show-top-level))
  :config
  (which-key-mode))

(use-package with-editor
  :ensure t
  :commands (with-editor-export-editor with-editor-shell-command with-editor-async-shell-command)
  :init
  (shell-command-with-editor-mode)
  (dolist (hook '(shell-mode-hook term-exec-hook eshell-mode-hook))
    (dolist (envvar '("EDITOR" "GIT_EDITOR"))
      (add-hook hook (apply-partially #'with-editor-export-editor envvar)))))

(use-package web-mode
  :ensure t
  :mode ("\\.html?\\'"
	 "\\.phtml\\'"
	 "\\.php\\'"
	 "\\.[agj]sp\\'"
	 "\\.as[cp]x\\'"
	 "\\.erb\\'"
	 "\\.mustache\\'"))

(use-package wgrep
  :ensure t
  :demand t
  :after (grep)
  :init
  (progn
    (setq wgrep-enable-key [remap read-only-mode])
    (setq wgrep-auto-save-buffer t)))

(progn ;     startup
  (message "Loading %s...done (%.3fs)" user-init-file
           (float-time (time-subtract (current-time)
                                      before-user-init-time)))
  (add-hook 'after-init-hook
            (lambda ()
              (message
               "Loading %s...done (%.3fs) [after-init]" user-init-file
               (float-time (time-subtract (current-time)
                                          before-user-init-time))))
            t))

(progn ;     personalize
  (let ((file (expand-file-name (concat (user-real-login-name) ".el")
                                user-emacs-directory)))
    (when (file-exists-p file)
      (load file))))

;; Local Variables:
;; indent-tabs-mode: nil
;; End:
;;; init.el ends here
